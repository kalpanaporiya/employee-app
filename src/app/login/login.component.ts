import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { EmployeeService } from '../Service/employee.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  constructor(private service: EmployeeService, private router: Router) {}

  ngOnInit(): void {}
  onLogin(form: NgForm) {
    this.service.Login(form.value).subscribe(
      (res: any) => {
        localStorage.setItem('Token', res.token);
        this.router.navigate(['employee-list']);
      },
      (err) => {
        console.log(err);
      }
    );
  }
}
