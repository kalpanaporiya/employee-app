import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { EmployeeService } from 'src/app/Service/employee.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css'],
})
export class EmployeeComponent implements OnInit {
  constructor(
    private router: Router,
    private datePipe: DatePipe,
    private service: EmployeeService
  ) {}

  DepartmentList = [
    { lable: 'Sales', value: 'sales' },
    { lable: 'Purchase', value: 'purchase' },
    { lable: 'Finance', value: 'finance' },
    { lable: 'Marketing', value: 'marketing' },
    { lable: 'Human Resource', value: 'human_resource' },
    { lable: 'Operations', value: 'operations' },
    { lable: 'General Management', value: 'general_management' },
    { lable: 'Development', value: 'development' },
  ];
  selectedDepartment = 'selectedDept';
  currentDate = new Date();
  hireDate: any;
  token = localStorage.getItem('Token')

  ngOnInit(): void {
    if (!this.token) {
      this.router.navigate(['/']);
    }
    this.hireDate = this.datePipe.transform(this.currentDate, 'yyyy-MM-dd');
    console.log(this.hireDate);
  }
  onSubmit(form: NgForm) {
    const requestPayload = {
      fields: [
        {
          element_name: 'emp_name',
          value: form.value.emp_name,
        },
        {
          element_id: '284373646',
          value: form.value.department,
        },
        {
          element_name: 'job_name',
          value: form.value.job_name,
        },
        {
          element_name: 'hire_date',
          value: this.hireDate,
        },
        {
          element_name: 'salary',
          value: form.value.salary,
        },
      ],
    };
    console.log(requestPayload);

    this.service.createEmployee(requestPayload).subscribe(
      (res: any) => {
        console.log(res);
        this.router.navigate(['employee-list']);
      },
      (err) => {
        console.log(err);
      }
    );
  }

  onBack() {
    this.router.navigate(['employee-list']);
  }
}
