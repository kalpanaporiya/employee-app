import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { EmployeeService } from 'src/app/Service/employee.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css'],
})
export class EmployeeListComponent implements OnInit {
  persons = [];
  constructor(private service: EmployeeService, private router: Router) { }
  token = localStorage.getItem('Token')
  ngOnInit(): void {
    if (!this.token) {
      this.router.navigate(['/']);
    }
   this.getEmployeeData()
  }
  getEmployeeData() {
    this.service.getEmployeeData().subscribe(
      (res: any) => {
        console.log(res);
        this.persons = res.response;
      },
      (err) => {
        console.log(err);
      }
    );
  }

  onAddEmployee() {
    this.router.navigate(['new-employee']);
  }
}
