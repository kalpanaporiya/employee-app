import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class EmployeeService {
  baseUrl = environment.apiUrl;
  token = localStorage.getItem('Token');
  constructor(private httpclient: HttpClient) {}
  Login(data: any): Observable<any[]> {
    return this.httpclient.post<any[]>(this.baseUrl + '/api/login', data);
  }

  getEmployeeData(): Observable<any[]> {
    console.log(this.token);
    return this.httpclient.get<any[]>(
      this.baseUrl + '/api/retrieve-list-of-records',
      {
        headers: {
          'content-type': 'application/json',
          Authorization: 'Bearer' + ' ' + this.token,
        },
      }
    );
  }

  createEmployee(data: any): Observable<any[]> {
    return this.httpclient.post<any[]>(
      this.baseUrl + '/api/add-new-records',
      data,
      {
        headers: {
          'content-type': 'application/json',
          Authorization: 'Bearer' + ' ' + this.token,
        },
      }
    );
  }
}
